package com.moneytransfer.service.account;

import com.moneytransfer.domain.Account;
import com.moneytransfer.models.AccountDto;
import com.moneytransfer.repository.account.AccountRepository;
import com.moneytransfer.service.exception.AccountCreateException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @Mock
    private AccountRepository accountRepository;

    @InjectMocks
    private AccountService accountService;

    @Test
    public void shouldSuccessfullyCreateAccount() {
        AccountDto accountDto = new AccountDto();
        accountDto.setPersonId(1l);
        accountDto.setAccountNumber(50l);
        accountDto.setSortCode(40l);
        accountDto.setBalance(new BigDecimal(100));

        ArgumentCaptor<Account> captor = ArgumentCaptor.forClass(Account.class);
        when(accountRepository.create(captor.capture())).thenReturn(1);

        accountService.createAccount(accountDto);

        verify(accountRepository, times(1)).create(any(Account.class));
        assertEquals(captor.getValue().getPersonId(), accountDto.getPersonId());
        assertEquals(captor.getValue().getAccountNumber(), accountDto.getAccountNumber());
        assertEquals(captor.getValue().getSortCode(), accountDto.getSortCode());
        assertEquals(captor.getValue().getBalance(), accountDto.getBalance());
    }

    @Test(expected = AccountCreateException.class)
    public void shouldThrowExceptionIfCreateAccountLessThan0() {
        AccountDto accountDto = new AccountDto();
        accountDto.setPersonId(1l);
        accountDto.setAccountNumber(50l);
        accountDto.setSortCode(40l);
        accountDto.setBalance(new BigDecimal(-1));
        accountService.createAccount(accountDto);

        verifyZeroInteractions(accountRepository);
    }

    @Test
    public void shouldSuccessfullyGetAccount() {
        Long accountNumber = 800l;
        Long sortCode = 29l;
        Account account = Account.builder()
                .id(1l)
                .accountNumber(accountNumber)
                .sortCode(sortCode)
                .personId(2l)
                .balance(BigDecimal.TEN)
                .build();
        when(accountRepository.get(accountNumber, sortCode)).thenReturn(account);

        Account accountReturned = accountService.getAccount(accountNumber, sortCode);

        assertEquals(account.getId(), accountReturned.getId());
        assertEquals(account.getPersonId(), accountReturned.getPersonId());
        assertEquals(account.getAccountNumber(), accountReturned.getAccountNumber());
        assertEquals(account.getSortCode(), accountReturned.getSortCode());
        assertEquals(account.getBalance(), accountReturned.getBalance());
    }

    @Test
    public void shouldSuccessfullyUpdateAccountBalance() {
        Account account1 = Account.builder()
                .id(1l)
                .accountNumber(50l)
                .sortCode(600l)
                .personId(1l)
                .balance(BigDecimal.TEN)
                .build();

        Account account2 = Account.builder()
                .id(2l)
                .accountNumber(501l)
                .sortCode(6010l)
                .personId(2l)
                .balance(BigDecimal.TEN)
                .build();

        accountService.updateAccountBalances(Arrays.asList(account1, account2));

        verify(accountRepository, times(2)).updateAccountBalance(any(Account.class));
    }
}
