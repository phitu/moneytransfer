package com.moneytransfer.service.transaction;

import com.moneytransfer.domain.Account;
import com.moneytransfer.domain.Transaction;
import com.moneytransfer.domain.TransactionType;
import com.moneytransfer.models.TransactionDto;
import com.moneytransfer.repository.transaction.TransactionRepository;
import com.moneytransfer.service.account.AccountService;
import com.moneytransfer.service.exception.DoesNotExistException;
import com.moneytransfer.service.exception.MoneyTransactionException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTest {

    @Mock
    private AccountService accountService;

    @Mock
    private TransactionRepository transactionRepository;

    @InjectMocks
    private TransactionService transactionService;

    @Test
    public void shouldSuccessfullyTransferFundsFromOneAccountToAnother() {
        Account account1 = createAccount(1l, 1l, 400l, 300l, new BigDecimal(300));
        Account account2 = createAccount(2l, 2l, 4000l, 3000l, new BigDecimal(200));

        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setFromAccountNumber(account1.getAccountNumber());
        transactionDto.setFromSortCode(account1.getSortCode());
        transactionDto.setTransactionType(TransactionType.TRANSFER.name());
        transactionDto.setAmount(new BigDecimal(30));
        transactionDto.setTimestamp(new Date());
        transactionDto.setToAccountNumber(account2.getAccountNumber());
        transactionDto.setToSortCode(account2.getSortCode());

        when(accountService.getAccount(transactionDto.getFromAccountNumber(), transactionDto.getFromSortCode())).thenReturn(account1);
        when(accountService.getAccount(transactionDto.getToAccountNumber(), transactionDto.getToSortCode())).thenReturn(account2);

        ArgumentCaptor<Transaction> transactionCaptor = ArgumentCaptor.forClass(Transaction.class);
        when(transactionRepository.create(transactionCaptor.capture())).thenReturn(1);

        transactionService.transferFunds(transactionDto);

        ArgumentCaptor<List<Account>> accountCaptor = ArgumentCaptor.forClass((Class)List.class);
        Mockito.verify(accountService, times(1)).updateAccountBalances(accountCaptor.capture());
        assertEquals(accountCaptor.getValue().size(), 2);
        assertEquals(accountCaptor.getValue().get(0).getBalance(), new BigDecimal(270));
        assertEquals(accountCaptor.getValue().get(1).getBalance(), new BigDecimal(230));

        assertEquals(transactionCaptor.getValue().getFromAccountId(), account1.getId());
        assertEquals(transactionCaptor.getValue().getToAccountId(), account2.getId());
        assertEquals(transactionCaptor.getValue().getTransactionType().name(), transactionDto.getTransactionType());
        assertEquals(transactionCaptor.getValue().getAmount(), transactionDto.getAmount());
    }

    @Test
    public void shouldSuccessfullyTransferFundsIfAccountHas0RemainderFromTransfer() {
        Account account1 = createAccount(1l, 1l, 400l, 300l, new BigDecimal(30));
        Account account2 = createAccount(2l, 2l, 4000l, 3000l, new BigDecimal(200));

        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setFromAccountNumber(account1.getAccountNumber());
        transactionDto.setFromSortCode(account1.getSortCode());
        transactionDto.setTransactionType(TransactionType.TRANSFER.name());
        transactionDto.setAmount(new BigDecimal(30));
        transactionDto.setTimestamp(new Date());
        transactionDto.setToAccountNumber(account2.getAccountNumber());
        transactionDto.setToSortCode(account2.getSortCode());

        when(accountService.getAccount(transactionDto.getFromAccountNumber(), transactionDto.getFromSortCode())).thenReturn(account1);
        when(accountService.getAccount(transactionDto.getToAccountNumber(), transactionDto.getToSortCode())).thenReturn(account2);

        ArgumentCaptor<Transaction> transactionCaptor = ArgumentCaptor.forClass(Transaction.class);
        when(transactionRepository.create(transactionCaptor.capture())).thenReturn(1);

        transactionService.transferFunds(transactionDto);

        ArgumentCaptor<List<Account>> accountCaptor = ArgumentCaptor.forClass((Class)List.class);
        Mockito.verify(accountService, times(1)).updateAccountBalances(accountCaptor.capture());
        assertEquals(accountCaptor.getValue().size(), 2);
        assertEquals(accountCaptor.getValue().get(0).getBalance(), account1.getBalance());
        assertEquals(accountCaptor.getValue().get(1).getBalance(), account2.getBalance());

        assertEquals(transactionCaptor.getValue().getFromAccountId(), account1.getId());
        assertEquals(transactionCaptor.getValue().getToAccountId(), account2.getId());
        assertEquals(transactionCaptor.getValue().getTransactionType().name(), transactionDto.getTransactionType());
        assertEquals(transactionCaptor.getValue().getAmount(), transactionDto.getAmount());
    }

    @Test(expected = DoesNotExistException.class)
    public void shouldThrowExceptionIfAccountNotFound() {
        Account account1 = createAccount(1l, 1l, 400l, 300l, new BigDecimal(200));
        Account account2 = createAccount(2l, 2l, 4000l, 3000l, new BigDecimal(200));

        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setFromAccountNumber(account1.getAccountNumber());
        transactionDto.setFromSortCode(account1.getSortCode());
        transactionDto.setTransactionType(TransactionType.TRANSFER.name());
        transactionDto.setAmount(new BigDecimal(30));
        transactionDto.setTimestamp(new Date());
        transactionDto.setToAccountNumber(account2.getAccountNumber());
        transactionDto.setToSortCode(account2.getSortCode());

        when(accountService.getAccount(transactionDto.getFromAccountNumber(), transactionDto.getFromSortCode())).thenReturn(account1);
        when(accountService.getAccount(transactionDto.getToAccountNumber(), transactionDto.getToSortCode())).thenReturn(null);

        transactionService.transferFunds(transactionDto);

        verify(accountService, times(0)).updateAccountBalances(any(List.class));
        verifyZeroInteractions(transactionRepository);
    }

    @Test(expected = MoneyTransactionException.class)
    public void shouldThrowExceptionIfNotEnoughFundsInAccountToProcessTransaction() {
        Account account1 = createAccount(1l, 1l, 400l, 300l, new BigDecimal(20));
        Account account2 = createAccount(2l, 2l, 4000l, 3000l, new BigDecimal(200));

        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setFromAccountNumber(account1.getAccountNumber());
        transactionDto.setFromSortCode(account1.getSortCode());
        transactionDto.setTransactionType(TransactionType.TRANSFER.name());
        transactionDto.setAmount(new BigDecimal(50));
        transactionDto.setTimestamp(new Date());
        transactionDto.setToAccountNumber(account2.getAccountNumber());
        transactionDto.setToSortCode(account2.getSortCode());

        when(accountService.getAccount(transactionDto.getFromAccountNumber(), transactionDto.getFromSortCode())).thenReturn(account1);
        when(accountService.getAccount(transactionDto.getToAccountNumber(), transactionDto.getToSortCode())).thenReturn(account2);

        transactionService.transferFunds(transactionDto);

        verify(accountService, times(0)).updateAccountBalances(any(List.class));
        verifyZeroInteractions(transactionRepository);
    }

    @Test
    public void shouldGetTransactionsForAccount() {
        Account account1 = createAccount(1l, 1l, 400l, 300l, new BigDecimal(20));
        Transaction transaction = Transaction.builder().id(1l).build();

        when(accountService.getAccount(account1.getAccountNumber(), account1.getSortCode())).thenReturn(account1);
        when(transactionService.getTransactions(account1.getAccountNumber(), account1.getSortCode())).thenReturn(Arrays.asList(transaction));

        List<Transaction> transactionList = transactionService.getTransactions(account1.getAccountNumber(), account1.getSortCode());

        assertEquals(transactionList.size(), 1);
        assertEquals(transactionList.get(0).getId(), transaction.getId());
    }

    @Test (expected = DoesNotExistException.class)
    public void shouldThrowExceptionIfAccountNotFoundForGetTransactions() {
        when(accountService.getAccount(anyLong(), anyLong())).thenReturn(null);

        List<Transaction> transactionList = transactionService.getTransactions(2l, 892l);

        verifyZeroInteractions(transactionRepository);
    }

    private Account createAccount(Long id, Long personId, Long accountNumber, Long sortCode, BigDecimal balance) {
        return Account.builder()
                .id(id)
                .personId(personId)
                .accountNumber(accountNumber)
                .sortCode(sortCode)
                .balance(balance)
                .build();
    }
}
