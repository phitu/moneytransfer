package com.moneytransfer.service.person;

import com.moneytransfer.domain.Person;
import com.moneytransfer.models.PersonDto;
import com.moneytransfer.repository.person.PersonRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PersonServiceTest {

    @Mock
    private PersonRepository personRepository;

    @InjectMocks
    private PersonService personService;

    @Test
    public void successfullyCreatePerson() {
        Integer personId = 1;
        String firstname = "John";
        String surname = "Smith";
        PersonDto personDto = new PersonDto();
        personDto.setFirstname(firstname);
        personDto.setSurname(surname);

        ArgumentCaptor<Person> captor = ArgumentCaptor.forClass(Person.class);
        when(personRepository.create(captor.capture())).thenReturn(personId);

        personService.createPerson(personDto);

        verify(personRepository, times(1)).create(any(Person.class));
        assertEquals(captor.getValue().getFirstname(), firstname);
        assertEquals(captor.getValue().getSurname(), surname);
    }

    @Test
    public void shouldGetPerson() {
        Integer personId = 1;
        String firstname = "John";
        String surname = "Smith";
        PersonDto personDto = new PersonDto();
        personDto.setFirstname(firstname);
        personDto.setSurname(surname);

        Person person = Person.builder()
                .firstname(firstname)
                .surname(surname)
                .build();

        when(personRepository.get(firstname, surname)).thenReturn(person);

        Person returnedPerson = personService.getPerson(firstname, surname);

        assertEquals(returnedPerson.getFirstname(), person.getFirstname());
        assertEquals(returnedPerson.getSurname(), person.getSurname());
    }
}
