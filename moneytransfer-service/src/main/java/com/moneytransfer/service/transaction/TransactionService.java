package com.moneytransfer.service.transaction;

import com.moneytransfer.domain.Account;
import com.moneytransfer.domain.Transaction;
import com.moneytransfer.domain.TransactionType;
import com.moneytransfer.models.TransactionDto;
import com.moneytransfer.repository.transaction.TransactionRepository;
import com.moneytransfer.service.account.AccountService;
import com.moneytransfer.service.exception.DoesNotExistException;
import com.moneytransfer.service.exception.MoneyTransactionException;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class TransactionService {

    private final TransactionRepository transactionRepository;
    private final AccountService accountService;

    public TransactionService(TransactionRepository transactionRepository, AccountService accountService) {
        this.transactionRepository = transactionRepository;
        this.accountService = accountService;
    }

    public int transferFunds(TransactionDto transactionDto) {

        Account account1 = retrieveAccount(transactionDto.getFromAccountNumber(), transactionDto.getFromSortCode());
        Account account2 = retrieveAccount(transactionDto.getToAccountNumber(), transactionDto.getToSortCode());

        BigDecimal newBalanceOfAccount1 = account1.getBalance().subtract(transactionDto.getAmount());
        BigDecimal newBalanceOfAccount2 = account2.getBalance().add(transactionDto.getAmount());
        if(newBalanceOfAccount1.compareTo(BigDecimal.ZERO) < 0 ) {
            throw new MoneyTransactionException("Not enough funds in account to process transaction");
        }

        account1.setBalance(newBalanceOfAccount1);
        account2.setBalance(newBalanceOfAccount2);
        accountService.updateAccountBalances(Arrays.asList(account1, account2));

        Transaction transaction = Transaction.builder()
                .fromAccountId(account1.getId())
                .toAccountId(account2.getId())
                .transactionType(TransactionType.valueOfIgnoreCase(transactionDto.getTransactionType()))
                .amount(transactionDto.getAmount())
                .timestamp(transactionDto.getTimestamp())
                .build();

        return transactionRepository.create(transaction);
    }

    public List<Transaction> getTransactions(Long accountNumber, Long sortCode) {
        Account account = accountService.getAccount(accountNumber, sortCode);
        if(account == null) {
            throw new DoesNotExistException("Unable to find account supplied");
        }

        return transactionRepository.getAllTransactions(account.getId());
    }

    private Account retrieveAccount(Long accountNumber, Long sortCode) {
        Account account = accountService.getAccount(accountNumber, sortCode);
        if(account == null) {
            throw new DoesNotExistException("Account not found");
        }
        return account;
    }
}
