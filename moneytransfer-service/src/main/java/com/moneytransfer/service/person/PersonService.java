package com.moneytransfer.service.person;

import com.moneytransfer.domain.Person;
import com.moneytransfer.models.PersonDto;
import com.moneytransfer.repository.person.PersonRepository;

public class PersonService {

    private final PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public int createPerson(PersonDto personDto) {
        Person person = Person.builder()
                .firstname(personDto.getFirstname())
                .surname(personDto.getSurname())
                .build();

        return personRepository.create(person);
    }

    public Person getPerson(String firstname, String surname) {
        return personRepository.get(firstname, surname);
    }

}
