package com.moneytransfer.service.account;

import com.moneytransfer.domain.Account;
import com.moneytransfer.models.AccountDto;
import com.moneytransfer.repository.account.AccountRepository;
import com.moneytransfer.service.exception.AccountCreateException;

import java.math.BigDecimal;
import java.util.List;

public class AccountService {

    private final AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public int createAccount(AccountDto accountDto) {
        if(accountDto.getBalance().compareTo(BigDecimal.ZERO) < 0) {
            throw new AccountCreateException("Cannot create account with below zero balance");
        }
        Account account = Account.builder()
                .personId(accountDto.getPersonId())
                .accountNumber(accountDto.getAccountNumber())
                .sortCode(accountDto.getSortCode())
                .balance(accountDto.getBalance())
                .build();

        return accountRepository.create(account);
    }

    public Account getAccount(Long accountNumber, Long sortCode) {
        Account account = accountRepository.get(accountNumber, sortCode);
        return account;
    }

    public void updateAccountBalances(List<Account> accounts) {
        accounts.forEach(a->accountRepository.updateAccountBalance(a));
    }
}
