package com.moneytransfer.service.exception;

public class AccountCreateException extends RuntimeException {
    public AccountCreateException(String message) {
        super(message);
    }
}
