package com.moneytransfer.service.exception;

public class MoneyTransactionException extends RuntimeException {
    public MoneyTransactionException(String message){
        super(message);
    }
}
