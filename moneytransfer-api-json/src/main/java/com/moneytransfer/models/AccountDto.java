package com.moneytransfer.models;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class AccountDto {

    private Long id;
    private Long personId;
    private Long accountNumber;
    private Long sortCode;
    private BigDecimal balance;

}
