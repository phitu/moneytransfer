package com.moneytransfer.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonDto {

    private long id;
    private String firstname;
    private String surname;

}
