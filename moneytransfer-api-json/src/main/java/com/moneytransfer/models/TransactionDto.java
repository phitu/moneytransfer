package com.moneytransfer.models;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class TransactionDto {

    private Long id;
    private Long fromAccountNumber;
    private Long fromSortCode;
    private String transactionType;
    private BigDecimal amount;
    private Date timestamp;
    private Long toAccountNumber;
    private Long toSortCode;

}
