package com.moneytransfer.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Builder(toBuilder = true)
public class Account {

    private Long id;

    private Long personId;

    private Long accountNumber;

    private Long sortCode;

    private BigDecimal balance;

}
