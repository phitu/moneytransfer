package com.moneytransfer.domain;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Builder
public class Transaction {

    private Long id;

    private Long fromAccountId;

    private Long toAccountId;

    private TransactionType transactionType;

    private BigDecimal amount;

    private Date timestamp;

}
