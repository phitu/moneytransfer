package com.moneytransfer.domain;

public enum TransactionType {

    DEPOSIT("Deposit"),
    WITHDRAW("Withdraw"),
    TRANSFER("Transfer");

    private String transactionType;

    TransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public static TransactionType valueOfIgnoreCase(String name) {
        if(name == null) {
            throw new IllegalArgumentException("Transaction type is not valid");
        }

        for(TransactionType value : values()) {
            if(value.name().equalsIgnoreCase(name))
                return value;
        }
        throw new IllegalArgumentException("Transaction type is not valid");
    }

}
