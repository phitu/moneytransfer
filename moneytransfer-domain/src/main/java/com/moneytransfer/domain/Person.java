package com.moneytransfer.domain;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder(toBuilder = true)
public class Person {

    private long id;

    private String firstname;

    private String surname;

}
