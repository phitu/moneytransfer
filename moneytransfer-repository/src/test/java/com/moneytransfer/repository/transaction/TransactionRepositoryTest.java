package com.moneytransfer.repository.transaction;

import com.moneytransfer.domain.Transaction;
import com.moneytransfer.domain.TransactionType;
import com.moneytransfer.repository.RepositoryTestHelper;
import com.moneytransfer.repository.transaction.dao.TransactionDao;
import org.hamcrest.core.Is;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

public class TransactionRepositoryTest extends RepositoryTestHelper {

    private TransactionDao transactionDao = new TransactionDao(jdbcAgent);
    private TransactionRepository transactionRepository = new TransactionRepositoryImpl(transactionDao);

    @Test
    public void shouldSuccessfullyCreateTransaction() {
        Transaction transaction = Transaction.builder()
                .fromAccountId(10l)
                .toAccountId(100l)
                .transactionType(TransactionType.TRANSFER)
                .amount(new BigDecimal(10))
                .timestamp(new Date())
                .build();
        transactionRepository.create(transaction);

        List<Transaction> transactionList = transactionRepository.getAllTransactions(10l);
        assertThat(transactionList.size(), Is.is(1));

    }
}
