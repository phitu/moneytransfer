package com.moneytransfer.repository.person;

import com.moneytransfer.domain.Person;
import com.moneytransfer.repository.person.dao.PersonDao;
import com.moneytransfer.repository.RepositoryTestHelper;
import org.hamcrest.core.Is;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class PersonRepositoryTest extends RepositoryTestHelper {

    private PersonDao personDao = new PersonDao(jdbcAgent);
    private PersonRepository personRepository = new PersonRepositoryImpl(personDao);

    @Test
    public void shouldSuccessfullyCreatePerson() {
        String firstname = "John";
        String surname = "Smith";
        createPerson(firstname, surname);

        Person person = personRepository.get(firstname, surname);

        assertThat(person.getFirstname(), Is.is(firstname));
        assertThat(person.getSurname(), Is.is(surname));

    }

    private Person createPerson(String firstname ,String surname) {
        Person person = Person.builder()
                .firstname(firstname)
                .surname(surname)
                .build();

        personRepository.create(person);

        person = personRepository.get(person.getFirstname(), person.getSurname());
        return person;
    }
}
