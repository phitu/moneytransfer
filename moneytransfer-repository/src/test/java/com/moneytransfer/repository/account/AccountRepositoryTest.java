package com.moneytransfer.repository.account;

import com.moneytransfer.domain.Account;
import com.moneytransfer.domain.Person;
import com.moneytransfer.repository.person.PersonRepository;
import com.moneytransfer.repository.person.PersonRepositoryImpl;
import com.moneytransfer.repository.person.dao.PersonDao;
import com.moneytransfer.repository.RepositoryTestHelper;
import com.moneytransfer.repository.account.dao.AccountDao;
import org.h2.jdbc.JdbcSQLException;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;
import java.sql.SQLException;

public class AccountRepositoryTest extends RepositoryTestHelper {

    private PersonDao personDao = new PersonDao(jdbcAgent);
    private PersonRepository personRepository = new PersonRepositoryImpl(personDao);

    private AccountDao accountDao = new AccountDao(jdbcAgent);
    private AccountRepository accountRepository = new AccountRepositoryImpl(accountDao);

    @Test
    public void createAccountShouldReturnNewlyCreatedAccount() {
        Person person = createPerson();

        Account account = createAccount(person);

        Account accountReturned = accountRepository.get(account.getAccountNumber(), account.getSortCode());
        assertThat(accountReturned.getAccountNumber(), is(account.getAccountNumber()));
        assertThat(accountReturned.getPersonId(), is(person.getId()));
        assertThat(accountReturned.getSortCode(), is(account.getSortCode()));
        assertThat(accountReturned.getBalance().stripTrailingZeros(), is(account.getBalance().stripTrailingZeros()));
    }

    @Test
    public void shouldNotCreateAccountIfAccountNumberAndSortCodeExists() {
        Person person = createPerson();
        Account account1 = Account.builder().personId(person.getId()).accountNumber(90l).sortCode(9l).build();
        Account account2 = Account.builder().personId(person.getId()).accountNumber(90l).sortCode(9l).build();

        Integer create1 = accountRepository.create(account1);
        Integer create2 = accountRepository.create(account2);

        assertEquals(create1, new Integer(1));
        assertEquals(create2, null);
    }

    @Test
    public void shouldSuccessfullyUpdateAccountBalance() {
        BigDecimal newBalance = new BigDecimal(900);
        Person person = createPerson();

        Account account = createAccount(person);
        Account accountReturned = accountRepository.get(account.getAccountNumber(), account.getSortCode());
        accountReturned.setBalance(newBalance);
        accountRepository.updateAccountBalance(accountReturned);

        Account a1 = accountReturned = accountRepository.get(account.getAccountNumber(), account.getSortCode());
        assertThat(a1.getBalance().stripTrailingZeros(), is(newBalance.stripTrailingZeros()));
    }

    private Account createAccount(Person person) {
        Account account = Account.builder()
                .personId(person.getId())
                .accountNumber(900L)
                .balance(new BigDecimal(100d))
                .sortCode(10l)
                .build();
        accountRepository.create(account);

        return account;
    }

    private Person createPerson() {
        Person person = Person.builder()
                .firstname("John")
                .surname("Smith")
                .build();

        personRepository.create(person);

        person = personRepository.get(person.getFirstname(), person.getSurname());
        return person;
    }
}
