package com.moneytransfer.repository;

import com.moneytransfer.repository.core.JdbcAgent;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;

public abstract class RepositoryTestHelper {

    protected static JdbcAgent jdbcAgent;

    @BeforeClass
    public static void setupDatabase() throws IOException, SQLException {

        jdbcAgent = new JdbcAgent();
        jdbcAgent.openConnection();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream in = loader.getResourceAsStream("MoneyTransfer.sql");
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sql = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sql.append(line);
        }
        jdbcAgent.executeQuery("DROP ALL OBJECTS");
        jdbcAgent.executeQuery(sql.toString());
        jdbcAgent.closeConnection();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        jdbcAgent.closeConnection();
    }
}
