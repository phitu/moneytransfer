package com.moneytransfer.repository.core;

import com.moneytransfer.repository.core.exception.UnsupportedRepositoryMethodException;

import java.util.List;

public interface QueryRepository <T, K> {

    default List<T> get() {
        throw new UnsupportedRepositoryMethodException("not implemented");
    }

    default T get(K key) {
        throw new UnsupportedRepositoryMethodException("not implemented");
    }

}

