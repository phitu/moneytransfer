package com.moneytransfer.repository.core.exception;

import com.moneytransfer.repository.core.JdbcAgent;

public class JdbcException extends RuntimeException {
    public JdbcException(String message) {
        super(message);
    }
}
