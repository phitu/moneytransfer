package com.moneytransfer.repository.core;

import com.moneytransfer.repository.core.exception.UnsupportedRepositoryMethodException;

public interface UpdateRepository <T, K> {

    default K create(T domainObjectInstance) { throw new UnsupportedRepositoryMethodException("not implemented"); };

}