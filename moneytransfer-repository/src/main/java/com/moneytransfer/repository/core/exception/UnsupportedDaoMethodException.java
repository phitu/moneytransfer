package com.moneytransfer.repository.core.exception;

public class UnsupportedDaoMethodException extends RuntimeException {
    public UnsupportedDaoMethodException(String message) {
        super(message);
    }
}