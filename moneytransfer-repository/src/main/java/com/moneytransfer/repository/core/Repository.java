package com.moneytransfer.repository.core;

public interface Repository <T, K> extends QueryRepository<T, K>, UpdateRepository<T, K> {
}
