package com.moneytransfer.repository.core;

import com.moneytransfer.repository.core.exception.UnsupportedDaoMethodException;

import java.util.List;

public interface Dao <T, K> {

    default List<T> get() {
        throw new UnsupportedDaoMethodException("not implemented");
    };

    default K create(final T domainObjectInstance) {
        throw new UnsupportedDaoMethodException("not implemented");
    };
}
