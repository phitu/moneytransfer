package com.moneytransfer.repository.core;

import com.moneytransfer.repository.core.exception.JdbcException;
import org.h2.jdbcx.JdbcDataSource;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcAgent {

    private static final String JDBC_DRIVER = "org.h2.Driver";
    private static final String DB_URL = "jdbc:h2:~/test";
    private static final String USER = "sa";
    private static final String PASS = "";
    private static Connection conn;
    private static Statement stmt;

    public JdbcAgent() {
        try {

            createDatabase();
        } catch(Exception e) {
            throw new JdbcException("Error whilst opening connection");
        }
    }

    public void openConnection() {
        try{
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
        } catch(Exception e) {
            throw new JdbcException("Error whilst opening connection");
        }
    }

    public PreparedStatement getPreparedStatement(String sql) throws SQLException {
        return conn.prepareStatement(sql);
    }

    public Long executeQuery(String sql) throws SQLException {
        return Long.valueOf(stmt.executeUpdate(sql));
    }

    public ResultSet executeResultSetQuery(PreparedStatement preparedStmt) throws SQLException {
        return preparedStmt.executeQuery();
    }

    public void closeConnection() {
        try {
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new JdbcException("Error whilst closing connection");
        }
    }

    public int executeQuery(PreparedStatement preparedStmt) throws SQLException {
        return preparedStmt.executeUpdate();
    }

    private void createDatabase() {
        try {
            openConnection();
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            InputStream in = loader.getResourceAsStream("MoneyTransfer.sql");
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder sql = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sql.append(line);
            }
            executeQuery("DROP ALL OBJECTS");
            executeQuery(sql.toString());
        } catch(SQLException | IOException e) {
            throw new RuntimeException("Error occurred whilst creating database." + e.getStackTrace());
        }
        finally {
            closeConnection();
        }
    }
}
