package com.moneytransfer.repository.core.exception;

public class UnsupportedRepositoryMethodException extends RuntimeException {
    public UnsupportedRepositoryMethodException(String message) {
        super(message);
    }
}