package com.moneytransfer.repository.account.dao;

import com.moneytransfer.domain.Account;
import com.moneytransfer.repository.core.Dao;
import com.moneytransfer.repository.core.JdbcAgent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountDao implements Dao<Account, Integer> {

    private final JdbcAgent jdbcAgent;

    public AccountDao(JdbcAgent jdbcAgent) {
        this.jdbcAgent = jdbcAgent;
    }

    @Override
    public Integer create(final Account account) {
        Integer query = null;
        final String sql = "INSERT INTO account (id, personid, accountnumber, sortcode, balance)" +
                " VALUES (SEQ_ACCOUNT.nextval, ?, ?, ?, ?) ";
        try {
            jdbcAgent.openConnection();
            PreparedStatement createAccountStmt = jdbcAgent.getPreparedStatement(sql);
            createAccountStmt.setLong(1, account.getPersonId());
            createAccountStmt.setLong(2, account.getAccountNumber());
            createAccountStmt.setLong(3, account.getSortCode());
            createAccountStmt.setBigDecimal(4, account.getBalance());

            query = jdbcAgent.executeQuery(createAccountStmt);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            jdbcAgent.closeConnection();
            return query;
        }
    }

    public Account get(Long accountNumber, Long sortCode) {
        Account account = null;
        final String sql = "SELECT id, personid, accountnumber, sortcode, balance " +
                "FROM account WHERE accountnumber = ? AND sortcode = ?";
        try {
            jdbcAgent.openConnection();
            PreparedStatement getStmt = jdbcAgent.getPreparedStatement(sql);
            getStmt.setLong(1, accountNumber);
            getStmt.setLong(2, sortCode);

            ResultSet rs = jdbcAgent.executeResultSetQuery(getStmt);

            while (rs.next()) {
                account = Account.builder()
                        .id(rs.getLong(1))
                        .personId(rs.getLong(2))
                        .accountNumber(rs.getLong(3))
                        .sortCode(rs.getLong(4))
                        .balance(rs.getBigDecimal(5))
                        .build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            jdbcAgent.closeConnection();
            return account;
        }
    }

    public Account get(Long id) {
        Account account = null;
        final String sql = "SELECT id, personid, accountnumber, sortcode, balance " +
                "FROM account WHERE id = ?";
        try {
            jdbcAgent.openConnection();
            PreparedStatement getStmt = jdbcAgent.getPreparedStatement(sql);
            getStmt.setLong(1, id);

            ResultSet rs = jdbcAgent.executeResultSetQuery(getStmt);
            while (rs.next()) {
                account = Account.builder()
                        .id(rs.getLong(1))
                        .personId(rs.getLong(2))
                        .accountNumber(rs.getLong(3))
                        .sortCode(rs.getLong(4))
                        .balance(rs.getBigDecimal(5))
                        .build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            jdbcAgent.closeConnection();
            return account;
        }
    }

    public Integer updateBalance(Account account) {
        Integer query = null;
        final String sql = "UPDATE account SET balance = ? WHERE id = ?";
        try {
            jdbcAgent.openConnection();
            PreparedStatement updateBalanceStmt = jdbcAgent.getPreparedStatement(sql);
            updateBalanceStmt.setBigDecimal(1, account.getBalance());
            updateBalanceStmt.setLong(2, account.getId());
            query = jdbcAgent.executeQuery(updateBalanceStmt);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            jdbcAgent.closeConnection();
            return query;
        }
    }

}
