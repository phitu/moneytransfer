package com.moneytransfer.repository.account;

import com.moneytransfer.domain.Account;

public interface AccountRepository {

    Integer create(Account account);

    Account get(Long accountNumber, Long sortCode);

    Account get(Long id);

    void updateAccountBalance(Account account);
}
