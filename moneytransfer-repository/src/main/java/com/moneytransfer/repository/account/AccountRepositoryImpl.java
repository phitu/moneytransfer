package com.moneytransfer.repository.account;

import com.moneytransfer.domain.Account;
import com.moneytransfer.repository.account.dao.AccountDao;

public class AccountRepositoryImpl implements AccountRepository {

    private final AccountDao accountDao;

    public AccountRepositoryImpl(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public Integer create(Account account) {
        return accountDao.create(account);
    }

    @Override
    public Account get(Long accountNumber, Long sortCode) {
        return accountDao.get(accountNumber, sortCode);
    }

    @Override
    public Account get(Long id) {
        return accountDao.get(id);
    }

    @Override
    public void updateAccountBalance(Account account) {
        accountDao.updateBalance(account);
    }

}
