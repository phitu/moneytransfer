package com.moneytransfer.repository.transaction;

import com.moneytransfer.domain.Transaction;
import com.moneytransfer.repository.transaction.dao.TransactionDao;

import java.util.List;

public class TransactionRepositoryImpl implements TransactionRepository {

    private final TransactionDao transactionDao;

    public TransactionRepositoryImpl(TransactionDao transactionDao) {
        this.transactionDao = transactionDao;
    }

    @Override
    public Integer create(Transaction transaction) {
        return transactionDao.create(transaction);
    }

    @Override
    public List<Transaction> getAllTransactions(Long accountNumber) {
        return transactionDao.getAllTransactionsForAccount(accountNumber);
    }
}
