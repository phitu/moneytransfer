package com.moneytransfer.repository.transaction;

import com.moneytransfer.domain.Transaction;

import java.util.List;

public interface TransactionRepository {
    Integer create(Transaction transaction);

    List<Transaction> getAllTransactions(Long accountNumber);

}
