package com.moneytransfer.repository.transaction.dao;

import com.moneytransfer.domain.Account;
import com.moneytransfer.domain.Transaction;
import com.moneytransfer.domain.TransactionType;
import com.moneytransfer.repository.core.Dao;
import com.moneytransfer.repository.core.JdbcAgent;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class TransactionDao implements Dao<Transaction, Integer> {

    private final JdbcAgent jdbcAgent;

    public TransactionDao(JdbcAgent jdbcAgent) {
        this.jdbcAgent = jdbcAgent;
    }

    public Integer create(Transaction transaction) {
        Integer query = null;
        final String sql = "INSERT INTO transaction (id, fromaccountid, toaccountid, transactiontype, amount, timestamp)" +
                " VALUES (SEQ_TRANSACTION.nextval, ?, ?, ?, ?, ?) ";
        try {
            jdbcAgent.openConnection();
            PreparedStatement createTransactionStmt = jdbcAgent.getPreparedStatement(sql);
            createTransactionStmt.setLong(1, transaction.getFromAccountId());
            createTransactionStmt.setLong(2, transaction.getToAccountId());
            createTransactionStmt.setString(3, String.valueOf(transaction.getTransactionType()));
            createTransactionStmt.setBigDecimal(4, transaction.getAmount());
            createTransactionStmt.setTimestamp(5, new Timestamp(transaction.getTimestamp().getTime()));

            query = jdbcAgent.executeQuery(createTransactionStmt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            jdbcAgent.closeConnection();
            return query;
        }
    }

    public List<Transaction> getAllTransactionsForAccount(Long accountId) {
        List<Transaction> transactions = new ArrayList<>();
        final String sql = "SELECT id, fromaccountid, toaccountid, transactiontype, amount, timestamp" +
                " FROM transaction WHERE fromaccountid = ?";
        try {
            jdbcAgent.openConnection();
            PreparedStatement getTransactionStmt = jdbcAgent.getPreparedStatement(sql);
            getTransactionStmt.setLong(1, accountId);

            ResultSet rs = jdbcAgent.executeResultSetQuery(getTransactionStmt);
            while (rs.next()) {
                Transaction transaction = Transaction.builder()
                        .id(rs.getLong(1))
                        .toAccountId(rs.getLong(2))
                        .fromAccountId(rs.getLong(3))
                        .transactionType(TransactionType.valueOf(rs.getString(4)))
                        .amount(rs.getBigDecimal(5))
                        .timestamp(rs.getTimestamp(6))
                        .build();
                transactions.add(transaction);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            jdbcAgent.closeConnection();
            return transactions;
        }
    }

}
