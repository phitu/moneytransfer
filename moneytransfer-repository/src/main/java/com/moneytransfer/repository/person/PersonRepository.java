package com.moneytransfer.repository.person;

import com.moneytransfer.domain.Person;

public interface PersonRepository {

    Integer create(Person person);

    Person get(String firstname, String surname) ;
}
