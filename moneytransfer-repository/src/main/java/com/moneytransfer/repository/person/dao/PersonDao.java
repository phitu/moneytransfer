package com.moneytransfer.repository.person.dao;

import com.moneytransfer.domain.Person;
import com.moneytransfer.repository.core.Dao;
import com.moneytransfer.repository.core.JdbcAgent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class PersonDao implements Dao<Person, Integer> {

    private final JdbcAgent jdbcAgent;

    public PersonDao(JdbcAgent jdbcAgent) {
        this.jdbcAgent = jdbcAgent;
    }

    @Override
    public Integer create(final Person person) {
        Integer query = null;
        final String sql = "INSERT INTO person (id, firstname, surname)" +
                " VALUES (SEQ_PERSON.nextval, ?, ?) ";
        try {
            jdbcAgent.openConnection();
            PreparedStatement createPersonStmt = jdbcAgent.getPreparedStatement(sql);
            createPersonStmt.setString(1, person.getFirstname());
            createPersonStmt.setString(2, person.getSurname());
            query = jdbcAgent.executeQuery(createPersonStmt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            jdbcAgent.closeConnection();
            return query;
        }
    }

    public Person get(String firstname, String surname) {
        Person person = null;
        final String sql = "SELECT id, firstname, surname FROM person" +
                " WHERE firstname = ? AND surname = ?";
        try {
            jdbcAgent.openConnection();
            PreparedStatement getStmt = jdbcAgent.getPreparedStatement(sql);
            getStmt.setString(1, firstname);
            getStmt.setString(2, surname);

            ResultSet rs = jdbcAgent.executeResultSetQuery(getStmt);

            while (rs.next()) {
                person = Person.builder()
                        .id(rs.getLong(1))
                        .firstname(rs.getString(2))
                        .surname(rs.getString(3))
                        .build();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            jdbcAgent.closeConnection();
            return person;
        }
    }
}
