package com.moneytransfer.repository.person;

import com.moneytransfer.domain.Person;
import com.moneytransfer.repository.person.dao.PersonDao;

public class PersonRepositoryImpl implements PersonRepository {

    private final PersonDao personDao;

    public PersonRepositoryImpl(PersonDao personDao) {
        this.personDao = personDao;
    }

    @Override
    public Integer create(Person person) {
        return personDao.create(person);
    }

    @Override
    public Person get(String firstname, String surname) {
        return personDao.get(firstname, surname);
    }
}
