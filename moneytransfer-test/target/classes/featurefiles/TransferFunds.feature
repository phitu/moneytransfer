Feature: Transfer funds from one account to another
  As a customer
  I want to be able to transfer funds
  So that I can transfer money from one account to another

  Scenario: successful transfer of monies from one account to another
    Given customers are created with first name, surname, account number, sort code and balance
      | FIRSTNAME | SURNAME | ACCOUNT_NUMBER | SORT_CODE | BALANCE |
      | John      | Doe     | 90008774       | 405684    | 350     |
      | Fred      | Banks   | 45556644       | 465523    | 200     |
    And an account transfers funds to another account
      | ACCOUNT_NUMBER1 | SORT_CODE1 | FUNDS | ACCOUNT_NUMBER2 | SORT_CODE2 |
      | 90008774        | 405684     | 20    | 45556644        | 465523     |
    Then the balances for the accounts should be updated
      | ACCOUNT_NUMBER | SORT_CODE | FINAL_BALANCE |
      | 90008774       | 405684    | 330           |
      | 45556644       | 465523    | 220           |
