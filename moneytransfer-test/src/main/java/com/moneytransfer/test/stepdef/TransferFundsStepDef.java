package com.moneytransfer.test.stepdef;

import com.moneytransfer.domain.Account;
import com.moneytransfer.domain.Person;
import com.moneytransfer.domain.TransactionType;
import com.moneytransfer.models.AccountDto;
import com.moneytransfer.models.PersonDto;
import com.moneytransfer.models.TransactionDto;
import com.moneytransfer.repository.account.AccountRepository;
import com.moneytransfer.repository.account.AccountRepositoryImpl;
import com.moneytransfer.repository.account.dao.AccountDao;
import com.moneytransfer.repository.core.JdbcAgent;
import com.moneytransfer.repository.person.PersonRepository;
import com.moneytransfer.repository.person.PersonRepositoryImpl;
import com.moneytransfer.repository.person.dao.PersonDao;
import com.moneytransfer.repository.transaction.TransactionRepository;
import com.moneytransfer.repository.transaction.TransactionRepositoryImpl;
import com.moneytransfer.repository.transaction.dao.TransactionDao;
import com.moneytransfer.service.account.AccountService;
import com.moneytransfer.service.person.PersonService;
import com.moneytransfer.service.transaction.TransactionService;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class TransferFundsStepDef {

    private final PersonService personService;
    private final PersonDao personDao;
    private final PersonRepository personRepository;
    private final AccountService accountService;
    private final AccountDao accountDao;
    private final AccountRepository accountRepository;
    private final TransactionService transactionService;
    private final TransactionDao transactionDao;
    private final TransactionRepository transactionRepository;
    private final JdbcAgent jdbcAgent;

    public TransferFundsStepDef() {
        jdbcAgent = new JdbcAgent();
        personDao = new PersonDao(jdbcAgent);
        personRepository = new PersonRepositoryImpl(personDao);
        personService = new PersonService(personRepository);
        accountDao = new AccountDao(jdbcAgent);
        accountRepository = new AccountRepositoryImpl(accountDao);
        accountService = new AccountService(accountRepository);
        transactionDao = new TransactionDao(jdbcAgent);
        transactionRepository = new TransactionRepositoryImpl(transactionDao);
        transactionService = new TransactionService(transactionRepository, accountService);
    }

    @Given("^customers are created with first name, surname, account number, sort code and balance$")
    public void customersAreCreatedWithFirstNameSurnameAccountNumberSortCodeAndBalance(DataTable dataTable) throws Throwable {
        dataTable.getPickleRows().stream().skip(1)
                .forEach(dataTableRow -> {

                    String firstname = String.valueOf(dataTableRow.getCells().get(0).getValue());
                    String surname = String.valueOf(dataTableRow.getCells().get(1).getValue());
                    Long accountNo = Long.valueOf(dataTableRow.getCells().get(2).getValue());
                    Long sortCode = Long.valueOf(dataTableRow.getCells().get(3).getValue());
                    BigDecimal balance = BigDecimal.valueOf(Double.valueOf(dataTableRow.getCells().get(4).getValue()));

                    PersonDto person = new PersonDto();
                    person.setFirstname(firstname);
                    person.setSurname(surname);

                    personService.createPerson(person);

                    Person personCreated = personService.getPerson(person.getFirstname(), person.getSurname());
                    AccountDto accountDto = new AccountDto();
                    accountDto.setPersonId(personCreated.getId());
                    accountDto.setAccountNumber(accountNo);
                    accountDto.setSortCode(sortCode);
                    accountDto.setBalance(balance);
                    accountService.createAccount(accountDto);

                });
    }

    @And("^an account transfers funds to another account$")
    public void anAccountTransfersFundsToAnotherAccount(DataTable dataTable) throws Throwable {
        dataTable.getPickleRows().stream().skip(1)
                .forEach(dataTableRow -> {
                    Long accountNo1 = Long.valueOf(dataTableRow.getCells().get(0).getValue());
                    Long sortCode1 = Long.valueOf(dataTableRow.getCells().get(1).getValue());
                    BigDecimal balance = BigDecimal.valueOf(Double.valueOf(dataTableRow.getCells().get(2).getValue()));
                    Long accountNo2 = Long.valueOf(dataTableRow.getCells().get(3).getValue());
                    Long sortCode2 = Long.valueOf(dataTableRow.getCells().get(4).getValue());

                    TransactionDto transactionDto = new TransactionDto();
                    transactionDto.setFromAccountNumber(accountNo1);
                    transactionDto.setFromSortCode(sortCode1);
                    transactionDto.setTransactionType(TransactionType.TRANSFER.name());
                    transactionDto.setAmount(balance);
                    transactionDto.setTimestamp(new Date());
                    transactionDto.setToAccountNumber(accountNo2);
                    transactionDto.setToSortCode(sortCode2);

                    transactionService.transferFunds(transactionDto);

                });
    }

    @Then("^the balances for the accounts should be updated$")
    public void theBalancesForTheAccountsShouldBeUpdated(DataTable dataTable) throws Throwable {
        dataTable.getPickleRows().stream().skip(1)
                .forEach(dataTableRow -> {
                    Long accountNo = Long.valueOf(dataTableRow.getCells().get(0).getValue());
                    Long sortCode = Long.valueOf(dataTableRow.getCells().get(1).getValue());
                    BigDecimal balance = BigDecimal.valueOf(Double.valueOf(dataTableRow.getCells().get(2).getValue()));

                    Account account = accountService.getAccount(accountNo, sortCode);

                    assertEquals(account.getBalance().stripTrailingZeros(), balance.stripTrailingZeros());
                });
    }
}

