package com.moneytransfer.test;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.junit.runner.RunWith;

@CucumberOptions(
        features = "classpath:featurefiles/TransferFunds.feature",
        glue = {""}
)
public class RunTest extends AbstractTestNGCucumberTests {

}
