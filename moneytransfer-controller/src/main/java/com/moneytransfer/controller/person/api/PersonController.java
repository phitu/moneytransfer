package com.moneytransfer.controller.person.api;

import com.moneytransfer.domain.Person;
import com.moneytransfer.models.PersonDto;
import com.moneytransfer.service.person.PersonService;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

@Path("/person")
public class PersonController {

    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addPerson(PersonDto personDto) {
        personService.createPerson(personDto);
        Person person = personService.getPerson(personDto.getFirstname(), personDto.getSurname());
        return Response.ok(person).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Person getPerson(@QueryParam("firstname") String firstname, @QueryParam("surname") String surname) {
        return personService.getPerson(firstname, surname);
    }

}
