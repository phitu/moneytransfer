package com.moneytransfer.controller.transaction.api;

import com.moneytransfer.domain.Account;
import com.moneytransfer.domain.Transaction;
import com.moneytransfer.models.TransactionDto;
import com.moneytransfer.service.transaction.TransactionService;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@Path("/transaction")
public class TransactionController {

    private final TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addTransaction(TransactionDto transactionDto) {
        transactionService.transferFunds(transactionDto);
        List<Transaction> transactions = transactionService.getTransactions(transactionDto.getFromAccountNumber(), transactionDto.getFromSortCode());
        Optional<Transaction> createdTransaction = transactions.stream().filter(a->a.getTimestamp().getTime()==transactionDto.getTimestamp().getTime()).findFirst();
        return Response.ok(createdTransaction.get()).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Transaction> getTransactions(@QueryParam("accountnumber") Long accountNo, @QueryParam("sortcode") Long sortCode) {
        return transactionService.getTransactions(accountNo, sortCode);
    }

}
