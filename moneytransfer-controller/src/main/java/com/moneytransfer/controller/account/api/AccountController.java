package com.moneytransfer.controller.account.api;

import com.moneytransfer.domain.Account;
import com.moneytransfer.models.AccountDto;
import com.moneytransfer.service.account.AccountService;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

@Path("/account")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addAccount(AccountDto accountDto) {
        accountService.createAccount(accountDto);
        Account accountCreated = accountService.getAccount(accountDto.getAccountNumber(), accountDto.getSortCode());
        return Response.ok(accountCreated).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccount(@QueryParam("accountnumber") Long accountNo, @QueryParam("sortcode") Long sortCode) {
        Account account = accountService.getAccount(accountNo, sortCode);
        if (account ==null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(account).build();
    }

}
