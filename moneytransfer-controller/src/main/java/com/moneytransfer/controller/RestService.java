package com.moneytransfer.controller;

import com.moneytransfer.controller.account.api.AccountController;
import com.moneytransfer.controller.person.api.PersonController;
import com.moneytransfer.controller.transaction.api.TransactionController;
import com.moneytransfer.repository.person.PersonRepositoryImpl;
import com.moneytransfer.repository.person.dao.PersonDao;
import com.moneytransfer.repository.account.AccountRepositoryImpl;
import com.moneytransfer.repository.account.dao.AccountDao;
import com.moneytransfer.repository.core.JdbcAgent;
import com.moneytransfer.repository.transaction.TransactionRepositoryImpl;
import com.moneytransfer.repository.transaction.dao.TransactionDao;
import com.moneytransfer.service.account.AccountService;
import com.moneytransfer.service.person.PersonService;
import com.moneytransfer.service.transaction.TransactionService;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@ApplicationPath("/moneytransfer")
public class RestService extends Application {
    private Set<Object> singletons = new HashSet<>();

    private final JdbcAgent jdbcAgent;

    public RestService() {
        jdbcAgent = new JdbcAgent();
        AccountService accountService = new AccountService(new AccountRepositoryImpl(new AccountDao(jdbcAgent)));
        singletons.add(new PersonController(new PersonService(new PersonRepositoryImpl(new PersonDao(jdbcAgent)))));
        singletons.add(new AccountController(accountService));
        singletons.add(new TransactionController(new TransactionService(new TransactionRepositoryImpl(new TransactionDao(jdbcAgent)), accountService)));
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

    @Override
    public Set<Class<?>> getClasses() {
        return super.getClasses();
    }

    @Override
    public Map<String, Object> getProperties() {
        return super.getProperties();
    }
}
